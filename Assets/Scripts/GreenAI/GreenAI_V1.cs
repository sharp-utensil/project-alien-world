using Pathfinding;
using Unity.Burst.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class GreenAI_V1 : MonoBehaviour
{
    // Debug
    [Header("Debug")]
    public bool SetDirectControl = false;
    public Behavior MonsterState;
    public enum Behavior { Test };
    public float y_velocity;
    public bool isGrounded;
    public float shortest_distance;
    public int best_direction;
    public float x_distance;
    public float y_distance;

    // Movement Input
    [Space]
    [Header("Movement Input")]
    public float moveInput;
    public bool dashInput;
    public bool jumpInputDown;
    public bool jumpInputUp;
    public bool attackInput;
    GreenAI_Movement movement;
    Hurtbox hurtbox;

    // AI Pathfind
    [Space]
    [Header("AI Pathfind")]
    AIDestinationSetter Destination;
    Path path;
    Path pathQueued;
    Seeker seeker;
    public float DistanceToCurrentTarget;
    public float nextWaypointDist = 5f;
    public int currentWaypoint = 0;
    public bool reachedEndOfPath = false;
    public float storedXDirection;

    // Life Functions
    LifeFunction lifeFunction;

    // Constants
    float RaycastToGroundLength = 12.0f;
    float RaycastFallSearchLength = 0.28f;

    private float queuedPathCooldown = 0;

    private void Start()
    {
        movement = GetComponent<GreenAI_Movement>();
        Destination = GetComponent<AIDestinationSetter>();
        seeker = GetComponent<Seeker>();
        hurtbox = GetComponentInChildren<Hurtbox>();
        lifeFunction = GetComponent<LifeFunction>();

        InvokeRepeating("UpdatePath", 0f, 0.2f);
    }

    void UpdatePath()
    {
        if (seeker.IsDone())
            seeker.StartPath(transform.position, Destination.target.transform.position, QueuePath);
    }

    void QueuePath(Path p)
    {
        if (isGrounded)
        {
            SetPath(p);
        }
        else
        {
            pathQueued = p;
        }
        
    }

    void SetPath(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    void FollowPath()
    {
        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        else
        {
            reachedEndOfPath = false;
        }

        float distance = Vector2.Distance(transform.position, path.vectorPath[currentWaypoint]);
        if (distance < nextWaypointDist && isGrounded)
        {
            currentWaypoint++;
        }
        else if (distance < 3.0f && Mathf.Abs(x_distance) < 0.25f && y_distance < -0.25f && y_velocity < 0)
        {
            currentWaypoint++;
        }

        if (currentWaypoint < path.vectorPath.Count) 
        {
            Move(path.vectorPath[currentWaypoint]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        movement.CheckIfGrounded();
        isGrounded = movement.isGrounded;
        y_velocity = movement.rb2d.velocity.y;

        // AI HERE
        // ========================================
        Pathfind();


        // ========================================

        if (Input.GetKeyDown(KeyCode.K))
        {
            SetDirectControl = !SetDirectControl;
        }

        if (SetDirectControl)
        {
            DirectControl();
        }
        
        if (!lifeFunction.isAlive)
        {
            movement.AIMove(0f, false, false, false, false);
        }
        else
        {
            movement.AIMove(moveInput, dashInput, jumpInputDown, jumpInputUp, attackInput);
        }
        
    }

    private void DirectControl()
    {
        moveInput = Input.GetAxisRaw("Horizontal");
        dashInput = Input.GetKey(KeyCode.LeftShift);
        jumpInputDown = Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.J);
        jumpInputUp = Input.GetButtonUp("Jump") || Input.GetKeyUp(KeyCode.J);
    }

    private void Pathfind()
    {
        if (isGrounded && movement.groundTime == 0.0f && queuedPathCooldown < 0.1 || reachedEndOfPath && y_velocity < -1f)
        {
            queuedPathCooldown = 1.0f;
            SetPath(pathQueued);
        }
        if (queuedPathCooldown > 0)
        {
            queuedPathCooldown -= Time.deltaTime;
        }
        FollowPath();
    }

    private void Move(Vector2 target)
    {
        x_distance = target.x - transform.position.x;
        y_distance = target.y - transform.position.y;
        float x_distance_abs = Mathf.Abs(x_distance);
        int x_direction = (int)Mathf.Sign(x_distance);
        float angle = Mathf.Atan2(y_distance, x_distance) * Mathf.Rad2Deg;
        if (angle > 0f)
        {
            angle = Mathf.Abs(angle - 90);
        }

        // Left and Right Input
        if (!isGrounded && y_velocity < 0f)
        {
            int best_location = ClosestGroundSearch(x_direction);
            if (movement.jumpsLeft == 0 || shortest_distance < x_distance_abs)
            {  
                if (best_location == 0)
                {
                    moveInput = 0;
                }
                else
                {
                    moveInput = Mathf.Sign(best_location);
                }
            }
            else
            {
                moveInput = x_direction;
            }
        }
        else if (!isGrounded && y_velocity > 1f && x_distance_abs < 0.05f)
        {
            moveInput = 0;
        }
        else
        {
            moveInput = x_direction;
        }
        
        // Jump Input
        if (isGrounded && y_distance > 0.1)
        {
            if (JumpOverFall(ClosestFallSearch(x_direction)))
            {
                jumpInputDown = true;
                storedXDirection = x_direction;
            }
            else if(angle < 35f && angle > 0f)
            {
                jumpInputDown = true;
                storedXDirection = x_direction;
            }
            else
            {
                jumpInputDown = false;
            }
        }
        else if (!isGrounded && y_distance > 0.1 && y_velocity < -3f && angle > 0f && movement.jumpsLeft > 0 && shortest_distance > x_distance_abs)
        {
            jumpInputDown = true;
        }
        else
        {
            jumpInputDown = false;
        }
        
        // Release Jump Input
        if (!isGrounded && y_velocity > 0f && x_distance_abs < 0.1f && y_distance < -0.75f)
        {
            jumpInputUp = true;
        }
        else { jumpInputUp = false; }

        // Dash
        if (x_distance_abs > 4.0f && y_distance < 0.1f)
        {
            moveInput = x_direction;
            dashInput = true;
        }
        else
        {
            dashInput = false;
        }

    }

    public bool JumpOverFall(int fall_num)
    {
        //Debug.Log(fall_num);
        if (fall_num == 1)
        {
            return true;
        }
        return false;
    }

    private int ClosestFallSearch(float x_direction)
    {

        Vector2 v_ONE = new Vector2(1 * x_direction, -1f);
        Vector2 v_TWO = new Vector2(2 * x_direction, -1f);
        Vector2 v_FOUR = new Vector2(4 * x_direction, -1f);
        Vector2 v_SIX = new Vector2(6 * x_direction, -1f);
        Vector2 v_EIGHT = new Vector2(8 * x_direction, -1f);

        Debug.DrawRay(transform.position, v_ONE * RaycastFallSearchLength, Color.red);
        Debug.DrawRay(transform.position, v_TWO * RaycastFallSearchLength, Color.red);
        Debug.DrawRay(transform.position, v_FOUR * RaycastFallSearchLength, Color.red);
        Debug.DrawRay(transform.position, v_SIX * RaycastFallSearchLength, Color.red);
        Debug.DrawRay(transform.position, v_EIGHT * RaycastFallSearchLength, Color.red);
        //Debug.DrawRay(transform.position, new Vector2(-1, -1) * RaycastFallSearchLength, Color.red);
        //Debug.DrawRay(transform.position, new Vector2(-2, -1) * RaycastFallSearchLength, Color.red);
        //Debug.DrawRay(transform.position, new Vector2(-4, -1) * RaycastFallSearchLength, Color.red);
        //Debug.DrawRay(transform.position, new Vector2(-6, -1) * RaycastFallSearchLength, Color.red);
        //Debug.DrawRay(transform.position, new Vector2(-8, -1) * RaycastFallSearchLength, Color.red);

        if (!RaycastFallSearch(v_ONE))
        {
            return 1;
        }
        else if (!RaycastFallSearch(v_TWO))
        {
            return 2;
        }
        else if (!RaycastFallSearch(v_FOUR))
        {
            return 4;
        }
        else if (!RaycastFallSearch(v_SIX))
        {
            return 6;
        }
        else if (!RaycastFallSearch(v_EIGHT))
        {
            return 8;
        }
        return 0;
    }

    private bool RaycastFallSearch(Vector2 vector)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, vector, (vector * RaycastFallSearchLength).magnitude, LayerMask.GetMask("Ground"));
        if (hit.collider != null)
        {
            float myAngle = Mathf.Abs(90f - (Vector3.Angle(hit.normal, Vector3.up)) - 90f);
            if (myAngle < movement.slopeLimit)
            {
                //Debug.Log(hit.collider.name);
                return true;
            }
        }
        return false;
    }

    private int ClosestGroundSearch(int x_direction)
    {
        Vector2 v_P75 = new Vector2(0.75f, -1f);
        Vector2 v_N75 = new Vector2(-0.75f, -1f);
        Vector2 v_P50 = new Vector2(0.5f, -1f);
        Vector2 v_N50 = new Vector2(-0.5f, -1f);
        Vector2 v_P25 = new Vector2(0.25f, -1f);
        Vector2 v_N25 = new Vector2(-0.25f, -1f);
        Vector2 v_P10 = new Vector2(0.1f, -1f);
        Vector2 v_N10 = new Vector2(-0.1f, -1f);
        Vector2 v_P04 = new Vector2(0.04f, -1f);
        Vector2 v_N04 = new Vector2(-0.04f, -1f);

        //Debug.DrawRay(transform.position, Vector2.down * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_P75 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_N75 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_P50 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_N50 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_P25 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_N25 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_P10 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_N10 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_P04 * RaycastToGroundLength, Color.blue);
        Debug.DrawRay(transform.position, v_N04 * RaycastToGroundLength, Color.blue);

        shortest_distance = 100f;
        best_direction = 0;
        float P04_Distance;
        float N04_Distance;

        if (RaycastValidGround(v_P04, out float distance_P1) && CheckShortestDistance(distance_P1))
        {
            best_direction = 1;
        }
        if (RaycastValidGround(v_P10, out float distance_P2) && CheckShortestDistance(distance_P2))
        {
            best_direction = 2;
        }
        if (RaycastValidGround(v_P25, out float distance_P3) && CheckShortestDistance(distance_P3))
        {
            best_direction = 3;
        }
        if (RaycastValidGround(v_P50, out float distance_P4) && CheckShortestDistance(distance_P4))
        {
            best_direction = 4;
        }
        if (RaycastValidGround(v_P75, out float distance_P5) && CheckShortestDistance(distance_P5))
        {
            best_direction = 5;
        }

        if (RaycastValidGround(v_N04, out float distance_N1) && CheckShortestDistance(distance_N1))
        {
            best_direction = -1;
        }
        if (RaycastValidGround(v_N10, out float distance_N2) && CheckShortestDistance(distance_N2))
        {
            best_direction = -2;
        }
        if (RaycastValidGround(v_N25, out float distance_N3) && CheckShortestDistance(distance_N3))
        {
            best_direction = -3;
        }
        if (RaycastValidGround(v_N50, out float distance_N4) && CheckShortestDistance(distance_N4))
        {
            best_direction = -4;
        }
        if (RaycastValidGround(v_N75, out float distance_N5) && CheckShortestDistance(distance_N5))
        {
            best_direction = -5;
        }

        P04_Distance = distance_P1;
        N04_Distance = distance_N1;
        if (Mathf.Abs(best_direction) == 1)
        {
            if (Mathf.Abs(P04_Distance - N04_Distance) < 0.1f)
            {
                if (y_velocity < 0f && y_velocity != -5f && storedXDirection != x_direction)
                {
                    best_direction = 0;
                }
                else
                {
                    best_direction = x_direction;
                }
            }
        }

        if (best_direction == 0)
        {
            //Debug.Log("NOTHING");
            best_direction = 0;
        }
        return best_direction;
    }

    private bool RaycastValidGround(Vector2 vector, out float distance)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, vector, (vector * RaycastToGroundLength).magnitude, LayerMask.GetMask("Ground"));
        if (hit.collider != null)
        {
            float myAngle = Mathf.Abs(90f - (Vector3.Angle(hit.normal, Vector3.up)) - 90f);
            if (myAngle < movement.slopeLimit)
            {
                //Debug.Log(hit.collider.name);
                distance = hit.distance;
                return true;
            }
        }
        distance = 0;
        return false;
    }

    private bool CheckShortestDistance(float distance)
    {
        if (distance > shortest_distance)
        {
            return false;
        }
        shortest_distance = distance;
        return true;
    }
}
