using System.Collections;
using System.Collections.Generic;
using System.IO.Compression;
using UnityEngine;

public class ArrowBoi2 : MonoBehaviour
{
    private GameObject player;
    private Rigidbody2D rb2d;

    public enum State { CloseRange, FarRange, CloseToFar, Zip };
    public State ArrowState;
    public float MaxStateChangeCooldown;
    public float CloseRangeSpeed;
    public float CloseRangeDistance;
    public float FarRangeSpeed;
    public float FarRangeDistance;
    public float ZipSpeed;
    public float ZipOvershoot;

    private float StateChangeCooldown;
    private float RotationModifier = 45.0f;
    private Vector3 tempPlayerPosition;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        ArrowState = State.FarRange;
    }

    // Update is called once per frame
    void Update()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        if (distanceToPlayer < 1 && ArrowState == State.Zip)
        {
            Debug.Log("Player Got Got");
            StateChangeCooldown = MaxStateChangeCooldown;
            ArrowState = State.CloseToFar;
        }

        switch (ArrowState)
        {
            case State.CloseRange:
                CloseRange(distanceToPlayer);
                break;
            case State.FarRange:
                FarRange(distanceToPlayer);
                break;
            case State.CloseToFar:
                CloseToFar(distanceToPlayer);
                break;
            case State.Zip:
                Zip(distanceToPlayer); 
                break;
            default:
                FarRange(distanceToPlayer);
                break;
        }
    }

    void CloseRange(float DTP)
    {
        if (DTP <= CloseRangeDistance)
        {
            Move(CloseRangeSpeed, player.transform.position);

            int LayerMask = 1;
            LayerMask = ~LayerMask;

            // Cast a ray straight down.
            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.rotation * new Vector3(1, 1, 1), 100f, LayerMask);
            //Debug.DrawRay(transform.position, transform.rotation * new Vector3(1, 1, 1) * 10, Color.green, 2f);

            // If it hits something...
            if (hit.collider != null)
            {
                Debug.DrawRay(hit.point, transform.rotation * new Vector3(1, 1, 1) * -hit.distance, Color.green, 0.1f);
                //Debug.Log(hit.collider.name);
                if (hit.collider.name == player.name) 
                {
                    
                    ArrowState = State.Zip;
                }
            }
            else
            {
                Debug.DrawRay(transform.position, transform.rotation * new Vector3(1, 1, 1) * 10, Color.red, 0.1f);
            }
        }
        else if (DTP <= FarRangeDistance)
        {
            ArrowState = State.CloseToFar;
        }
    }

    void FarRange(float DTP)
    {
        if (DTP <= CloseRangeDistance)
        {
            ArrowState = State.CloseRange;
        }
        else if (DTP <= FarRangeDistance)
        {
            Vector3 target = new Vector3(player.transform.position.x + CloseRangeDistance/2, player.transform.position.y + CloseRangeDistance/2);
            Move(FarRangeSpeed, target);
        }
    }

    void CloseToFar(float DTP)
    {
        Move(CloseRangeSpeed, player.transform.position);
        
        if (StateChangeCooldown > 0) 
        { 
            StateChangeCooldown -= Time.deltaTime; 
        }
        else 
        {
            StateChangeCooldown = MaxStateChangeCooldown;
            ArrowState = State.FarRange;
        }
    }

    void Zip(float DTP)
    {
        if (StateChangeCooldown == MaxStateChangeCooldown)
        {
            tempPlayerPosition = player.transform.position + (player.transform.position - transform.position) * ZipOvershoot;
        }
        if (StateChangeCooldown > 0)
        {
            StateChangeCooldown -= Time.deltaTime;
        }
        else if (MaxStateChangeCooldown - StateChangeCooldown > 0.3)
        {
            StateChangeCooldown = MaxStateChangeCooldown;
            ArrowState = State.CloseToFar;
        }

        float singleStep = ZipSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, tempPlayerPosition, singleStep);
    }

    void Move(float Speed, Vector3 target)
    {
        // Information variables
        Vector3 targetDirection = target - transform.position;
        float singleStep = Speed * Time.deltaTime;

        float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg - RotationModifier;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

        transform.rotation = Quaternion.Lerp(transform.rotation, q, singleStep);
        transform.position = Vector3.MoveTowards(transform.position, target, singleStep);
    }
}
