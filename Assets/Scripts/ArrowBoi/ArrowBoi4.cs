using UnityEngine;
using Pathfinding;


public class ArrowBoi4 : MonoBehaviour
{
    public GameObject target;
    private Rigidbody2D rb2d;
    private GameObject pivot;

    public enum State { Follow, Lunge, Search };
    public State ArrowState;
    public float FollowSpeed = 5;
    public float RotationSpeed = 60;
    public float FollowDistance = 11;
    public float LungeSpeed = 20;
    public float DistanceToTarget;
    public float GravityAir;
    public float GravityGround;

    //Move
    float RotationModifier = 45.0f;

    //Lunge
    Vector2 tempTargetPosition;
    bool Lunge_StruckObject;
    bool Lunge_PlayerGot;

    //Follow
    public float nextWaypointDist = 5f;
    Path path;
    Seeker seeker;
    int currentWaypoint = 0;
    bool reachedEndOfPath = false;


    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        ArrowState = State.Search;
        seeker = GetComponent<Seeker>();
        pivot = transform.Find("PivotPoint").gameObject;

        InvokeRepeating("UpdatePath", 0f, 0.2f);
    }

    void UpdatePath()
    {
        if(seeker.IsDone())
            seeker.StartPath(pivot.transform.position, target.transform.position, OnPathComplete);
    }

    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    void FollowPath()
    {
        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        else
        {
            reachedEndOfPath = false;
        }

        Move(FollowSpeed, path.vectorPath[currentWaypoint]);
        float distance = Vector2.Distance(pivot.transform.position, path.vectorPath[currentWaypoint]);

        if (distance < nextWaypointDist)
        {
            currentWaypoint++;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DistanceToTarget = Vector2.Distance(pivot.transform.position, target.transform.position);

        switch (ArrowState)
        {
            case State.Follow:
                Follow();
                break;
            case State.Lunge:
                Lunge(); 
                break;
            case State.Search:
                Search();
                break;
            default:
                Search();
                break;
        }

    }

    void Follow()
    {
        if (DistanceToTarget <= FollowDistance)
        {
            TargetRaycast(DistanceToTarget*1.5f, out bool UnobstructedLine);
            if(UnobstructedLine) 
            {
                //FollowPath();
                Move(FollowSpeed, target.transform.position);
                ForwardRaycast(FollowDistance, out bool TargetLocked);
                if (TargetLocked)
                {
                    Lunge_PlayerGot = false;
                    Lunge_StruckObject = false;
                    tempTargetPosition = target.transform.position;
                    //ArrowState = State.Lunge;
                }
            }
            else
            {
                FollowPath();
            }
        }
        else
        {
            ArrowState = State.Search;
        }
    }

    void Search()
    { 
        TargetRaycast(FollowDistance, out bool TargetDetected);
        if(TargetDetected)
        {
            ArrowState = State.Follow;
        }
    }

    void Lunge()
    {
        float distanceToTemp = Vector2.Distance(transform.position, tempTargetPosition);
        
        if (DistanceToTarget < 1)
        {
            Debug.Log("Player Got Got");
            Lunge_PlayerGot = true;
        }
        else if (Lunge_PlayerGot)
        {
            Move(FollowSpeed, target.transform.position);
            ArrowState = State.Follow;
        }
        else if (ForwardRaycast(0.7f, out bool TargetStruck).collider == null && Lunge_StruckObject == false)
        {
            Move(LungeSpeed, tempTargetPosition);
        }
        else
        {
            Lunge_StruckObject = true;
            ArrowState = State.Follow;
        }

        if(distanceToTemp < 1)
        {
            ArrowState = State.Follow;
        }
    }

    RaycastHit2D TargetRaycast(float distance, out bool TargetDetected)
    {
        int LayerMask = 1;
        LayerMask = ~LayerMask;
        TargetDetected = false;
        Vector3 pivotVector = pivot.transform.position;
        Vector2 direction = pivotVector - target.transform.position;

        float angle = Vector3.Angle(-direction, transform.rotation * new Vector2(1, 1));

        //Debug.Log(angle);

        // Cast a ray
        RaycastHit2D hit = Physics2D.Raycast(pivotVector, -direction.normalized, distance, LayerMask);

        // If it hits something...
        if (hit.collider != null)
        {
            //Debug.Log(hit.collider.gameObject);
            if (hit.collider.gameObject == target && angle < 35f)
            {
                Debug.DrawRay(pivotVector, direction.normalized * -hit.distance, Color.blue, 0.05f);
                TargetDetected = true;
            }
            else
            {
                Debug.DrawRay(pivotVector, direction.normalized * -10, Color.yellow, 0.05f);
            }
        }
        else
        {
            Debug.DrawRay(pivotVector, direction.normalized * -10, Color.yellow, 0.05f);
        }
        return hit;
    }
    RaycastHit2D ForwardRaycast(float distance, out bool TargetLocked) 
    {
        int LayerMask = 1;
        LayerMask = ~LayerMask;
        TargetLocked = false;

        // Cast a ray
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.rotation * new Vector2(1, 1), distance, LayerMask);

        // If it hits something...
        if (hit.collider != null)
        {
            Debug.DrawRay(hit.point, transform.rotation * new Vector2(1, 1) * -hit.distance, Color.green, 0.1f);
            //Debug.Log(hit.collider.name);
            if (hit.collider.gameObject == target)
            {
                TargetLocked = true;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.rotation * new Vector2(1, 1) * 10, Color.red, 0.1f);
        }
        return hit;
    }

    void Move(float Speed, Vector3 target)
    {
        // Information variables
        Vector2 targetDirection = target - transform.position;
        float singleStep = Speed * Time.deltaTime;
        float rotationStep = RotationSpeed * Time.deltaTime;

        float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg - RotationModifier;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

        Quaternion newRot = Quaternion.Slerp(transform.rotation, q, rotationStep).normalized;
        Vector3 newPos = Vector3.MoveTowards(transform.position, target, singleStep);
        rb2d.MoveRotation(newRot);
        rb2d.MovePosition(newPos);
        
        
    }
}
