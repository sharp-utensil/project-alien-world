using UnityEngine;
using Pathfinding;


public class ArrowBoi3 : MonoBehaviour
{
    public GameObject player;
    private Rigidbody2D rb2d;
    private GameObject pivot;

    public enum State { CloseRange, FarRange, CloseToFar, Zip, Search };
    public State ArrowState;
    public float MaxStateChangeCooldown = 2;
    public float CloseRangeSpeed = 3;
    public float CloseRangeDistance = 11;
    public float FarRangeSpeed = 40;
    public float FarRangeDistance = 50;
    public float ZipSpeed = 50;
    public float ZipOvershoot = 1.5f;
    public float nextWaypointDist = 5f;

    float StateChangeCooldown;
    float RotationModifier = 45.0f;
    float SearchRotation = 200.0f;
    Vector2 tempPlayerPosition;

    Path path;
    Seeker seeker;
    int currentWaypoint = 0;
    bool reachedEndOfPath = false;

    
   

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        ArrowState = State.Search;
        seeker = GetComponent<Seeker>();
        pivot = transform.Find("PivotPoint").gameObject;

        InvokeRepeating("UpdatePath", 0f, 0.2f);
    }

    void UpdatePath()
    {
        if(seeker.IsDone())
            seeker.StartPath(pivot.transform.position, player.transform.position, OnPathComplete);
    }

    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    void FollowPath()
    {
        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        else
        {
            reachedEndOfPath = false;
        }

        Move(CloseRangeSpeed, path.vectorPath[currentWaypoint]);
        float distance = Vector2.Distance(rb2d.position, path.vectorPath[currentWaypoint]);

        if (distance < nextWaypointDist)
        {
            currentWaypoint++;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
        if (distanceToPlayer < 1 && ArrowState == State.Zip)
        {
            Debug.Log("Player Got Got");
            StateChangeCooldown = MaxStateChangeCooldown;
            ArrowState = State.CloseToFar;
        }

        switch (ArrowState)
        {
            case State.CloseRange:
                CloseRange(distanceToPlayer);
                break;
            case State.FarRange:
                FarRange(distanceToPlayer);
                break;
            case State.CloseToFar:
                CloseToFar(distanceToPlayer);
                break;
            case State.Zip:
                Zip(); 
                break;
            case State.Search:
                Search(distanceToPlayer);
                break;
            default:
                FarRange(distanceToPlayer);
                break;
        }
    }

    void CloseRange(float DTP)
    {
        if (DTP <= CloseRangeDistance)
        {
            TargetRaycast(DTP*1.5f, out bool UnobstructedLine);
            if(UnobstructedLine) 
            {
                Move(CloseRangeSpeed, player.transform.position);
                ForwardRaycast(CloseRangeDistance, out bool PlayerHit);
                if (PlayerHit)
                {
                    ArrowState = State.Zip;
                }
            }
            else
            {
                FollowPath();
            }
        }
        else if (DTP <= FarRangeDistance)
        {
            ArrowState = State.CloseToFar;
        }
    }

    void FarRange(float DTP)
    {
        if (DTP <= CloseRangeDistance)
        {
            ArrowState = State.CloseRange;
        }
        else if (DTP <= FarRangeDistance)
        {
            Vector2 target = new Vector2(player.transform.position.x + CloseRangeDistance/2, player.transform.position.y + CloseRangeDistance/2);
            FollowPath();
        }
        else
        {
            ArrowState = State.Search;
        }

    }

    void Search(float DPT)
    { 
        transform.Rotate(0, 0, SearchRotation * Time.deltaTime);
        ForwardRaycast(FarRangeDistance, out bool PlayerHit);
        if(PlayerHit)
        {
            ArrowState = State.FarRange;
        }
    }

    RaycastHit2D TargetRaycast(float distance, out bool PlayerDetected)
    {
        int LayerMask = 1;
        LayerMask = ~LayerMask;
        PlayerDetected = false;
        Vector3 pivotVector = pivot.transform.position;
        Vector2 direction = pivotVector - player.transform.position;

        float angle = Vector3.Angle(-direction, transform.rotation * new Vector2(1, 1));

        //Debug.Log(angle);

        // Cast a ray
        RaycastHit2D hit = Physics2D.Raycast(pivotVector, -direction.normalized, distance, LayerMask);

        // If it hits something...
        if (hit.collider != null)
        {
            //Debug.Log(hit.collider.name);
            if (hit.collider.tag == "Player" && angle < 35f)
            {
                Debug.DrawRay(pivotVector, direction.normalized * -hit.distance, Color.blue, 0.05f);
                PlayerDetected = true;
            }
            else
            {
                Debug.DrawRay(pivotVector, direction.normalized * -10, Color.yellow, 0.05f);
            }
        }
        else
        {
            Debug.DrawRay(pivotVector, direction.normalized * -10, Color.yellow, 0.05f);
        }
        return hit;
    }
    RaycastHit2D ForwardRaycast(float distance, out bool PlayerDetected) 
    {
        int LayerMask = 1;
        LayerMask = ~LayerMask;
        PlayerDetected = false;

        // Cast a ray
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.rotation * new Vector2(1, 1), distance, LayerMask);

        // If it hits something...
        if (hit.collider != null)
        {
            Debug.DrawRay(hit.point, transform.rotation * new Vector2(1, 1) * -hit.distance, Color.green, 0.1f);
            //Debug.Log(hit.collider.name);
            if (hit.collider.tag == "Player")
            {
                PlayerDetected = true;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.rotation * new Vector2(1, 1) * 10, Color.red, 0.1f);
        }
        return hit;
    }

    void CloseToFar(float DTP)
    {
        Move(CloseRangeSpeed, player.transform.position);
        
        if (StateChangeCooldown > 0) 
        { 
            StateChangeCooldown -= Time.deltaTime; 
        }
        else 
        {
            StateChangeCooldown = MaxStateChangeCooldown;
            ArrowState = State.FarRange;
        }
    }

    private bool StruckObject;
    private bool PlayerGot;
    void Zip()
    {
        if (StateChangeCooldown == MaxStateChangeCooldown)
        {
            tempPlayerPosition = player.transform.position + (player.transform.position - transform.position) * ZipOvershoot;
            StruckObject = false;
            PlayerGot = false;
        }
        if (StateChangeCooldown > 0)
        {
            StateChangeCooldown -= Time.deltaTime;
        }
        else if (MaxStateChangeCooldown - StateChangeCooldown > 0.3)
        {
            StateChangeCooldown = MaxStateChangeCooldown;
            ArrowState = State.CloseToFar;
        }

        float distanceToTarget = Vector2.Distance(transform.position, tempPlayerPosition);
        if (distanceToTarget < 1) 
        {
            PlayerGot = true;
        }
        else if (PlayerGot)
        {
            Move(CloseRangeSpeed, player.transform.position);
        }
        else if (ForwardRaycast(0.7f, out bool PlayerStruck).collider == null && StruckObject == false)
        {
            Move(ZipSpeed, tempPlayerPosition);
        }
        else
        {
            StruckObject = true;
        }
    }

    void Move(float Speed, Vector3 target)
    {
        // Information variables
        Vector2 targetDirection = target - transform.position;
        float singleStep = Speed * Time.deltaTime;

        float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg - RotationModifier;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

        Quaternion newRot = Quaternion.Slerp(transform.rotation, q, singleStep).normalized;
        Vector3 newPos = Vector3.MoveTowards(transform.position, target, singleStep);
        rb2d.MoveRotation(newRot);
        rb2d.MovePosition(newPos);
        
    }
}
