using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowBoi1 : MonoBehaviour
{
    private GameObject player;
    private Rigidbody2D rb2d;

    public float Speed;
    public float Follow_Range;

    private float RotationModifier = 45.0f; 

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

        if (distanceToPlayer <= Follow_Range)
        {
            // Information variables
            Vector3 targetDirection = player.transform.position - transform.position;
            float singleStep = Speed * Time.deltaTime;

            float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg - RotationModifier;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

            transform.rotation = Quaternion.Lerp(transform.rotation, q, singleStep);
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, singleStep);
        }

    }
}
