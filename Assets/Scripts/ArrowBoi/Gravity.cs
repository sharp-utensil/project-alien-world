using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowBoi4GravitySetup : MonoBehaviour
{
    public ArrowBoi4 ArrowBoi4;
    private Rigidbody2D rb2d;
    public GameObject ForwardCell;

    public float GravityAir;
    public float GravityGround;
    public bool Colliding;
    public float Speed;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        GravityAir = ArrowBoi4.GravityAir;
        GravityGround = ArrowBoi4.GravityGround;
    }

    void Update()
    {
        GravityAir = ArrowBoi4.GravityAir;
        GravityGround = ArrowBoi4.GravityGround;

        /*if (GravityGround == ArrowBoi4.GravityGround)
        {
            Vector3 newPos = Vector3.MoveTowards(transform.position, ForwardCell.transform.position, Speed * Time.deltaTime);
            rb2d.MovePosition(newPos);
        }*/
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            rb2d.gravityScale = GravityGround;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            rb2d.gravityScale = GravityAir;
        }
    }
}
