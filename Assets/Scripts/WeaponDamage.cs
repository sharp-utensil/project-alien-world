using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public static class WeaponDamage
{
    public const int hornDamageInitial = 200;
    public const int hornDamagePenetration = 40;

}
